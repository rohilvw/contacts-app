package com.example.rohil.contactsapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.rohil.contactsapp.ContactsRoom.Contact;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rohil on 12/12/2017.
 */

public class ContactListAdapter extends RecyclerView.Adapter<ContactListAdapter.ViewHolder> {

    List<Contact> contactList;
    Context context;

    AdapterInterface adapterInterface;

    public void setContext(Context context) {
        this.context = context;

        try {
            adapterInterface = (AdapterInterface) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement AdapterInterface");
        }
    }

    public ContactListAdapter() {
        contactList = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.textViewName.setText(contactList.get(position).getName());
        holder.textViewPhone.setText(contactList.get(position).getPhoneNumber());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//
                Contact contact = new Contact(contactList.get(position).getName(), contactList.get(position).getPhoneNumber());
                contact.setContactId(contactList.get(position).getContactId());
                adapterInterface.contactPressed(contact);
            }
        });


    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView textViewName;
        public TextView textViewPhone;

        public ViewHolder(View itemView) {
            super(itemView);
            textViewName = (TextView)itemView.findViewById(R.id.textViewName);
            textViewPhone = (TextView)itemView.findViewById(R.id.textViewPhone);
        }
    }

    public void updateContacts (List<Contact> contacts){
        contactList.clear();
        contactList.addAll(contacts);
        notifyDataSetChanged();
    }

    public interface AdapterInterface{
        void contactPressed(Contact contact);
    }
}
