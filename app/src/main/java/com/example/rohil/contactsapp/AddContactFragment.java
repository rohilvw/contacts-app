package com.example.rohil.contactsapp;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.rohil.contactsapp.ContactsRoom.Contact;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AddContactFragment.AddFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class AddContactFragment extends Fragment {

    private AddFragmentInteractionListener mListener;

    EditText editTextName;
    EditText editTextPhone;
    Button buttonAddContact;

    public AddContactFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_contact, container, false);

        setRetainInstance(true);

        editTextName = (EditText)view.findViewById(R.id.editTextName);
        editTextPhone = (EditText)view.findViewById(R.id.editTextPhone);
        buttonAddContact = (Button)view.findViewById(R.id.buttonAddContact);

        buttonAddContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editTextName.setFreezesText(true);
                editTextPhone.setFreezesText(true);;
                Contact contact = new Contact(editTextName.getText().toString(), editTextPhone.getText().toString());
                onButtonPressed(contact);
            }
        });

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Contact contact) {
        if (mListener != null) {
            mListener.AddFragmentInteraction(contact);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof AddFragmentInteractionListener) {
            mListener = (AddFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface AddFragmentInteractionListener {
        // TODO: Update argument type and name
        void AddFragmentInteraction(Contact contact);
    }
}
