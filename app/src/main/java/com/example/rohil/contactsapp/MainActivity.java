package com.example.rohil.contactsapp;

import android.Manifest;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.rohil.contactsapp.ContactsRoom.Contact;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MainActivity extends AppCompatActivity
        implements AddContactFragment.AddFragmentInteractionListener,
        ListContactFragment.ListContactInterface, ContactListAdapter.AdapterInterface, ShowContactFragment.ShowContactFragmentInterface{

    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 785;
    private static final int MY_RESULT_STRING = 115;

    Cursor cursor;

    MainActivityViewModel mainActivityViewModel;

    LiveData<List<Contact>> listLiveData;

    ListContactFragment listContactFragment;

    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;

    AddContactFragment addContactFragment;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        fragmentManager = getSupportFragmentManager();





        mainActivityViewModel = ViewModelProviders.of(this).get(MainActivityViewModel.class);

        mainActivityViewModel.setContext(this);

        mainActivityViewModel.loadData();

        listLiveData = mainActivityViewModel.getListLiveData();



        listContactFragment = new ListContactFragment();
        listContactFragment.setListLiveData(listLiveData);

        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fragment_holder, listContactFragment);
        fragmentTransaction.commit();



        Log.v("MainActivity", "data is being observed: " + listLiveData.hasActiveObservers());


        SharedPreferences sharedPreferences = getPreferences(MODE_PRIVATE);
        int check = sharedPreferences.getInt("firstOpen", 0);
        if (check == 0) {
            Log.v("MainActivty", "Contacts not added");

            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.READ_CONTACTS)
                    != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.READ_CONTACTS)) {

                    Toast.makeText(this, "Required to add all contacts", Toast.LENGTH_SHORT).show();
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_CONTACTS},
                            MY_PERMISSIONS_REQUEST_READ_CONTACTS);

                } else {

                    // No explanation needed, we can request the permission.

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_CONTACTS},
                            MY_PERMISSIONS_REQUEST_READ_CONTACTS);

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            }
        } else {

            Log.v("MainActivity", "Contacts already added");
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    List<Contact> contacts = new ArrayList<>();
                    List<String> contactIds = new ArrayList<>();


                    cursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
                    while (cursor.moveToNext()) {
                        //String id = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone._ID));
                        String name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                        String number = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        number = number.replaceAll(" ", "");
                        if (contactIds.contains(number))
                            continue;
                        contactIds.add(number);
                        contacts.add(new Contact(name, number));

                    }

                    cursor.close();

                    mainActivityViewModel.addAllContacts(contacts);

                    Log.v("MainActivity", "size: " + contacts.size());

                    SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putInt("firstOpen", 1);
                    editor.commit();


                } else
                    Toast.makeText(this, "Permission not granted", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void AddFragmentInteraction(Contact contact) {
        mainActivityViewModel.addContact(contact);
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_holder, listContactFragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onAddButtonSelected() {
        fragmentTransaction = fragmentManager.beginTransaction();
        addContactFragment = new AddContactFragment();
        fragmentTransaction.replace(R.id.fragment_holder, addContactFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void contactPressed(Contact contact) {
        fragmentTransaction = fragmentManager.beginTransaction();
        mainActivityViewModel.setContact(contact);
        ShowContactFragment showContactFragment = new ShowContactFragment();
        showContactFragment.setContact(contact);
        fragmentTransaction.replace(R.id.fragment_holder, showContactFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void onContactDeleted(Contact contact) {
        mainActivityViewModel.deleteContact(contact);
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_holder, listContactFragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onContactEdited(Contact contact) {
        mainActivityViewModel.editContact(contact);
    }
}