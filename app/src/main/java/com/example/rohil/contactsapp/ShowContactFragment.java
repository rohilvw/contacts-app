package com.example.rohil.contactsapp;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.rohil.contactsapp.ContactsRoom.Contact;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ShowContactFragment.ShowContactFragmentInterface} interface
 * to handle interaction events.
 */
public class ShowContactFragment extends Fragment {

    private ShowContactFragmentInterface mListener;

    EditText textViewName;
    EditText textViewPhone;

    Contact contact;

    Button buttonEditContact;

    public void setContact(Contact contact) {
        //this.contact = contact;
    }

    public ShowContactFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_show_contact, container, false);

        setHasOptionsMenu(true);
        setRetainInstance(true);

        MainActivityViewModel mainActivityViewModel = ViewModelProviders.of(getActivity()).get(MainActivityViewModel.class);
        mainActivityViewModel.loadData();

        contact = mainActivityViewModel.getContact();

        textViewName = (EditText)view.findViewById(R.id.textViewName);
        textViewPhone = (EditText)view.findViewById(R.id.textViewPhone);
        buttonEditContact = (Button)view.findViewById(R.id.buttonEditContact);

        textViewName.setText(contact.getName());
        textViewPhone.setText(contact.getPhoneNumber());

        textViewPhone.setEnabled(false);
        textViewName.setEnabled(false);

        buttonEditContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Contact contact1 = new Contact(textViewName.getText().toString(), textViewPhone.getText().toString());
                contact1.setContactId(contact.getContactId());
                mListener.onContactEdited(contact1);

                textViewName.setEnabled(false);
                textViewPhone.setEnabled(false);
                buttonEditContact.setVisibility(View.GONE);

            }
        });




        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ShowContactFragmentInterface) {
            mListener = (ShowContactFragmentInterface) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement ShowContactFragmentInterface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface ShowContactFragmentInterface {
        // TODO: Update argument type and name
        void onContactDeleted(Contact contact);
        void onContactEdited (Contact contact);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        if (item.getItemId()==R.id.action_delete){
            mListener.onContactDeleted(contact);
        }

        if (item.getItemId() == R.id.action_edit){

            textViewName.setEnabled(true);
            textViewPhone.setEnabled(true);
            buttonEditContact.setVisibility(View.VISIBLE);

        }

        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.show_contact_menu, menu);

    }
}
