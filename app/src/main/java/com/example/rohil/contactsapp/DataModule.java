package com.example.rohil.contactsapp;

import android.arch.persistence.room.Room;
import android.content.Context;

import com.example.rohil.contactsapp.AppDatabase;
import com.example.rohil.contactsapp.ContactApplication;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Rohil on 12/19/2017.
 */


@Module
public class DataModule {

    private ContactApplication contactApplication;

    public DataModule(ContactApplication contactApplication) {
        this.contactApplication = contactApplication;
    }

    @Provides
    Context applicationContext() {
        return contactApplication;
    }

    @Provides
    @Singleton
    AppDatabase providesAppDatabase(Context context){
        return Room.databaseBuilder(context, AppDatabase.class, "contacts.db").build();
    }
}
