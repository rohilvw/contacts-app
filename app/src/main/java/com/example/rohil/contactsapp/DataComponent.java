package com.example.rohil.contactsapp;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Rohil on 12/19/2017.
 */

@Singleton
@Component(modules = {DataModule.class})
public interface DataComponent {

    void inject (MainActivityViewModel mainActivityViewModel);
}
