package com.example.rohil.contactsapp.ContactsRoom;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

/**
 * Created by Rohil on 12/6/2017.
 */

@Dao
public interface ContactDao {

    @Query("SELECT * FROM contacts ORDER BY name")
    List<Contact> getAll();

    @Query("SELECT * FROM contacts ORDER BY name")
    LiveData<List<Contact>> getAllLiveData();

    @Query("SELECT * FROM contacts WHERE contactId IN (:contactIds)")
    List<Contact> loadAllByIds(int[] contactIds);

    @Insert
    long insert (Contact contact);

    @Delete
    int delete(Contact contact);

    @Update
    int update(Contact contact);

}
