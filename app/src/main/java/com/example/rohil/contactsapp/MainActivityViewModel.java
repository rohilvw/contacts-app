package com.example.rohil.contactsapp;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import com.example.rohil.contactsapp.ContactsRoom.Contact;

import java.util.List;

/**
 * Created by Rohil on 12/18/2017.
 */

public class MainActivityViewModel extends ViewModel {

    LiveData<List<Contact>> listLiveData;
    AppDatabase appDatabase;
    Context context;

    Contact contact;

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {

        this.contact = contact;
    }

    public void setContext(Context context) {
        this.context = context;
        appDatabase = Room.databaseBuilder(context, AppDatabase.class, "contacts.db").build();
    }

    public void loadData(){

        listLiveData = appDatabase.contactDao().getAllLiveData();
        Log.v("MainActivityViewModel", "Live data created");
    }

    public LiveData<List<Contact>> getListLiveData() {
        if (listLiveData==null){

            return listLiveData;
        }
        return listLiveData;
    }

    public void addAllContacts(List<Contact> contacts){
        new AddContacts(contacts).execute();
    }

    public void addContact(Contact contact){
        new AddContactTask().execute(contact);
    }

    public void deleteContact(Contact contact){
        new DeleteConatctTask().execute(contact);
    }

    public void editContact (Contact contact){
        new EditConatatTask().execute(contact);
    }

    class AddContacts extends AsyncTask<Integer, Integer, Integer>{

        List<Contact> contactList;

        public AddContacts(List<Contact> contactList) {
            this.contactList = contactList;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            Log.v("MainActivityViewModel", "Contacts added");
//            new GetContacts().execute();
        }

        @Override
        protected Integer doInBackground(Integer... integers) {

            for (int i=0; i<contactList.size(); i++)
                appDatabase.contactDao().insert(contactList.get(i));

            return null;
        }
    }

    class AddContactTask extends AsyncTask<Contact, Void, Void>{
        @Override
        protected Void doInBackground(Contact... contacts) {

            long i = appDatabase.contactDao().insert(contacts[0]);
            Log.v("MainActivityViewModel", "Contact Added: "+i);
            return null;
        }
    }

    class DeleteConatctTask extends AsyncTask<Contact, Void, Void>{
        @Override
        protected Void doInBackground(Contact... contacts) {
            int i = appDatabase.contactDao().delete(contacts[0]);
            Log.v("MainActivityViewModel", "Contact deleted: "+i);
            return null;
        }
    }

    class EditConatatTask extends AsyncTask<Contact, Void, Void>{

        @Override
        protected Void doInBackground(Contact... contacts) {
            int i =appDatabase.contactDao().update(contacts[0]);
            Log.v("MainActivityViewModel", "rows updated: "+i);
            return null;
        }
    }




}
