package com.example.rohil.contactsapp;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.example.rohil.contactsapp.ContactsRoom.Contact;
import com.example.rohil.contactsapp.ContactsRoom.ContactDao;

/**
 * Created by Rohil on 12/6/2017.
 */

@Database(entities = {Contact.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase{
    public abstract ContactDao contactDao();
}
