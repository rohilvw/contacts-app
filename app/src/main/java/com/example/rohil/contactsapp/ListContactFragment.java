package com.example.rohil.contactsapp;


import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.rohil.contactsapp.ContactsRoom.Contact;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class ListContactFragment extends Fragment  {

    //LiveData<List<Contact>> listLiveData;

    RecyclerView recyclerView;

    ContactListAdapter contactListAdapter;

    ListContactInterface listContactInterface;

    public void setListLiveData(LiveData<List<Contact>> listLiveData) {
        //this.listLiveData = listLiveData;
    }

    public ListContactFragment() {
        // Required empty public constructor
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        if (item.getItemId()==R.id.action_add){
            listContactInterface.onAddButtonSelected();
        }

        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        menu.clear();
        inflater.inflate(R.menu.menu_main, menu);


        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_list_contact, container, false);

        setHasOptionsMenu(true);
        setRetainInstance(true);


        MainActivityViewModel mainActivityViewModel = ViewModelProviders.of(getActivity()).get(MainActivityViewModel.class);
        mainActivityViewModel.loadData();

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_show_contacts);

        contactListAdapter = new ContactListAdapter();
        contactListAdapter.setContext(getContext());

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(contactListAdapter);



        mainActivityViewModel.getListLiveData().observe(this, new Observer<List<Contact>>() {
            @Override
            public void onChanged(@Nullable List<Contact> contacts) {
                contactListAdapter.updateContacts(contacts);
            }
        });


        return view;

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            listContactInterface = (ListContactInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement ListContactInterface");
        }
    }

    public interface ListContactInterface{
        public void onAddButtonSelected();
    }

}
