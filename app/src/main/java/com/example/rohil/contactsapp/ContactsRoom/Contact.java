package com.example.rohil.contactsapp.ContactsRoom;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by Rohil on 12/6/2017.
 */

@Entity (tableName = "contacts")
public class Contact {
    public Contact(String name, String phoneNumber) {
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

    public void setContactId(int contactId) {
        this.contactId = contactId;
    }

    public int getContactId() {

        return contactId;
    }

    @PrimaryKey(autoGenerate = true)
    public int contactId;

    public String name;
    public String phoneNumber;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }


}
